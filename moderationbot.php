<?php
$admin = array ("225117913",);
if (strpos($msg, '>ban ') === 0 and in_array($userID, $admin))
  {
  try
    {
    $scan = $MadelineProto->get_info(explode(' ', $msg) [1]);
    $MadelineProto->channels->editBanned(['channel' => $chatID, 'user_id' => $scan['bot_api_id'], 'banned_rights' => ['_' => 'chatBannedRights', 'view_messages' => 1, 'send_messages' => 1, 'send_media' => 1, 'send_stickers' => 1, 'send_gifs' => 1, 'send_games' => 1, 'send_inline' => 1, 'embed_links' => 1, 'until_date' => 0], ]);
    sm($chatID, "Ho bannato " . $scan['User']['first_name'] . " [" . $scan['bot_api_id'] . "] come richiesto da $name [$userID].", 1);
    $nome = $scan['User']['first_name'];
    $id = $scan['bot_api_id'];
    }
  catch(Exception $e)
    {
    $errore = $e ->getMessage();
    $MadelineProto->messages->sendMessage(['peer' => $chatID, 'message' => $errore]);
    }
  }
	if (strpos($msg, '>unban ') === 0 and in_array($userID, $admin))
	  {
	  try
	    {
	    $scan = $MadelineProto->get_info(explode(' ', $msg) [1]);
	    $MadelineProto->channels->editBanned(['channel' => $chatID, 'user_id' => $scan['bot_api_id'], 'banned_rights' => ['_' => 'chatBannedRights', 'view_messages' => 0, 'send_messages' => 0, 'send_media' => 0, 'send_stickers' => 0, 'send_gifs' => 0, 'send_games' => 0, 'send_inline' => 0, 'embed_links' => 0, 'until_date' => 0], ]);
	    sm($chatID, "Ho Sbannato " . $scan['User']['first_name'] . " [" . $scan['bot_api_id'] . "] come richiesto da $name [$userID].");
	    $nome = $scan['User']['first_name'];
	    $id = $scan['bot_api_id'];
	    }
	  catch(Exception $e)
	    {
	    $errore = $e ->getMessage();
	    $MadelineProto->messages->sendMessage(['peer' => $chatID, 'message' => $errore]);
	    }
	  }
